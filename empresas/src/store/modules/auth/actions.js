export function signInRequest(email, password) {
  return {
    type: '@auth/SIGN_IN_REQUEST',
    payload: { email, password },
  };
}

export function signInSuccess(
  investor,
  enterprise,
  success,
  client,
  uid,
  accessToken
) {
  return {
    type: '@auth/SIGN_IN_SUCCESS',
    payload: { investor, enterprise, success, client, uid, accessToken },
  };
}

export function signInError(errors, success) {
  return {
    type: '@auth/SIGN_IN_ERROR',
    payload: { errors, success },
  };
}

import produce from 'immer';

const INITIAL_SATE = {
  investor: null,
  enterprise: null,
  success: false,
  client: null,
  uid: null,
  accessToken: null,
  isLoading: false,
};

export default function auth(state = INITIAL_SATE, action) {
  console.tron.log(action.payload);

  switch (action.type) {
    case '@auth/SIGN_IN_REQUEST':
      return produce(state, draft => {
        draft.isLoading = true;
      });

    case '@auth/SIGN_IN_SUCCESS':
      return produce(state, draft => {
        draft.investor = action.payload.investor;
        draft.enterprise = action.payload.enterprise;
        draft.success = action.payload.success;
        draft.client = action.payload.client;
        draft.uid = action.payload.uid;
        draft.accessToken = action.payload.accessToken;
        draft.isLoading = false;
      });
    default:
      return state;
  }
}

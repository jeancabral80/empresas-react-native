import { takeLatest, call, put, all } from 'redux-saga/effects';
import api from '~/services/api';

import { signInSuccess } from './actions';

export function* signIn({ payload }) {
  const { email, password } = payload;

  const response = yield call(api.post, '/users/auth/sign_in', {
    email,
    password,
  });

  const {
    data: { investor, enterprise, success },
    headers: { client, uid },
  } = response;

  const accessToken = response.headers['access-token'];

  yield put(
    signInSuccess(investor, enterprise, success, client, uid, accessToken)
  );
}

export default all([takeLatest('@auth/SIGN_IN_REQUEST', signIn)]);

export function getAllEnterpriseRequest() {
  return {
    type: '@enterprise/GET_ALL_ENTERPRISE_REQUEST',
    payload: {},
  };
}

export function getAllEnterpriseSuccess(enterprises) {
  return {
    type: '@enterprise/GET_ALL_ENTERPRISE_SUCCESS',
    payload: { enterprises },
  };
}

export function filterEnterpriseRequest(enterprise_type, name) {
  return {
    type: '@enterprise/FILTER_ENTERPRISE_REQUEST',
    payload: { enterprise_type, name },
  };
}

export function filterEnterpriseSuccess(enterprises) {
  return {
    type: '@enterprise/FILTER_ENTERPRISE_SUCCESS',
    payload: { enterprises },
  };
}

import produce from 'immer';

const INITIAL_SATE = {
  enterprises: [],
  isLoading: false,
};

export default function enterprise(state = INITIAL_SATE, action) {
  switch (action.type) {
    case '@enterprise/GET_ALL_ENTERPRISE_REQUEST':
      return produce(state, draft => {
        draft.isLoading = true;
      });

    case '@enterprise/GET_ALL_ENTERPRISE_SUCCESS':
      return produce(state, draft => {
        draft.enterprises = action.payload.enterprises;
        draft.isLoading = false;
      });

    case '@enterprise/FILTER_ENTERPRISE_REQUEST':
      return produce(state, draft => {
        draft.isLoading = true;
      });

    case '@enterprise/FILTER_ENTERPRISE_SUCCESS':
      return produce(state, draft => {
        draft.enterprises = action.payload.enterprises;
        draft.isLoading = false;
      });

    default:
      return state;
  }
}

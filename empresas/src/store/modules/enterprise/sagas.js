import { takeLatest, select, call, put, all } from 'redux-saga/effects';
import api from '~/services/api';

import { getAllEnterpriseSuccess, filterEnterpriseSuccess } from './actions';

export const getAuth = state => state.auth;

export function* getAll() {
  const auth = yield select(getAuth);

  const config = {
    headers: {
      'access-token': auth.accessToken,
      client: auth.client,
      uid: auth.uid,
    },
  };

  const response = yield call(api.get, '/enterprises', config);

  const {
    data: { enterprises },
  } = response;

  yield put(getAllEnterpriseSuccess(enterprises));
}

export function* filter({ payload }) {
  const { name, enterprise_type } = payload;

  const auth = yield select(getAuth);

  const config = {
    headers: {
      'access-token': auth.accessToken,
      client: auth.client,
      uid: auth.uid,
    },
  };

  const response = yield call(
    api.get,
    `/enterprises?name=${name}&enterprise_types=${enterprise_type}`,
    config
  );

  const {
    data: { enterprises },
  } = response;

  yield put(filterEnterpriseSuccess(enterprises));
}

export default all([
  takeLatest('@enterprise/GET_ALL_ENTERPRISE_REQUEST', getAll),
  takeLatest('@enterprise/FILTER_ENTERPRISE_REQUEST', filter),
]);

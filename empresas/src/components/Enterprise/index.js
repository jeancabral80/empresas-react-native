import React from 'react';
import { TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Feather';

import {
  ListItem,
  Card,
  Photo,
  Info,
  ItemName,
  ItemType,
  ItemOrigin,
} from './styles';

export default function Enterprise({ data, navigation }) {
  function handleNavigate() {
    const { id, enterprise_name, enterprise_type } = data;
    navigation.navigate('EnterpriseDetail', {
      id,
      enterprise_name,
      enterprise_type,
    });
  }

  return (
    <>
      <ListItem onPress={handleNavigate}>
        <Card>
          <Photo
            source={{
              uri: data.photo
                ? `http://empresas.ioasys.com.br/${data.photo}`
                : `https://ui-avatars.com/api/?rounded=true&name=${
                    data.enterprise_name
                  }`,
            }}
          />
          <Info>
            <ItemName>{data.enterprise_name}</ItemName>
            <ItemType>{data.enterprise_type.enterprise_type_name}</ItemType>
            <ItemOrigin>{`${data.city} (${data.country})`}</ItemOrigin>
          </Info>
        </Card>
        <TouchableOpacity onPress={handleNavigate}>
          <Icon name="chevron-right" size={18} color="#61045F" />
        </TouchableOpacity>
      </ListItem>
    </>
  );
}

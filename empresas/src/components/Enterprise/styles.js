import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';

export const Card = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 0 20px;
`;

export const Info = styled.View`
  margin-left: 14px;
  display: flex;
`;

export const Title = styled.Text`
  margin-bottom: 4px;
  font-size: 16px;
  font-weight: bold;
  line-height: 22px;
  color: #000;
`;

export const List = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
})`
  margin: 5px 0;
  padding: 10px 20px;
`;

export const Photo = styled.Image`
  width: 42px;
  height: 42px;
  border-radius: 21px;
`;

export const ListItem = styled(RectButton).attrs({
  shadowColor: '#f9f9f9',
  shadowOffset: {
    width: 1,
    height: 1,
  },
  shadowOpacity: 0.5,
  shadowRadius: 2,

  elevation: 1,
})`
  margin: 5px;
  padding: 10px;
  border-radius: 4px;
  background: #f9f9f9;
  flex-grow: 1;
  flex-basis: 0;
  height: 90px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const ItemName = styled.Text.attrs({
  numberOfLines: 1,
})`
  margin-bottom: 4px;
  font-size: 15px;
  font-weight: bold;
  color: #61045f;
`;

export const ItemType = styled.Text`
  margin-bottom: 4px;
  font-size: 13px;
  color: rgba(255, 20, 161, 1);
`;

export const ItemOrigin = styled.Text`
  font-size: 11px;
  color: #999;
`;

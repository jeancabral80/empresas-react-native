import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';

export default styled(LinearGradient).attrs({
  colors: ['rgba(255,20,161,1)', '#61045F'],
  start: { x: 0.2, y: 0.3 },
  end: { x: 1, y: 1 },
})`
  flex: 1;
`;

import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import { createStackNavigator } from 'react-navigation-stack';

import Main from './pages/Main';
import SignIn from './pages/SignIn';

import EnterpriseDetail from './pages/EnterpriseDetail';

export default (isAuth = false) =>
  createAppContainer(
    createSwitchNavigator(
      {
        Sign: createSwitchNavigator({
          SignIn,
        }),
        App: createStackNavigator(
          {
            Main,
            EnterpriseDetail,
          },
          {
            headerLayoutPreset: 'center',
            defaultNavigationOptions: {
              headerTransparent: true,
              headerTintColor: '#fff',
              headerLeftContainerStyle: {
                marginLeft: 10,
              },
              headerRightContainerStyle: {
                marginRight: 20,
              },
            },
          }
        ),
      },
      {
        initialRouteName: isAuth ? 'App' : 'Sign',
      }
    )
  );

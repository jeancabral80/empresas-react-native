import React from 'react';

import { useSelector } from 'react-redux';

import { YellowBox } from 'react-native';

import createRouter from './routes';

YellowBox.ignoreWarnings(['Warning:', 'Setting a timer']);

export default function App() {
  const isAuth = useSelector(state => state.auth.success);

  const Routes = createRouter(isAuth);

  return <Routes />;
}

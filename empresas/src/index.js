import React from 'react';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';

import './config/ReactotronConfig';

import store from './store';

import App from './App';

export default function Index() {
  return (
    <>
      <Provider store={store}>
        <StatusBar
          barStyle="light-content"
          backgroundColor="rgba(255,20,161,1)"
        />
        <App />
      </Provider>
    </>
  );
}

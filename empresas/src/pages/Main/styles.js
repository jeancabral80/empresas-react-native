import { Platform } from 'react-native';
import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';

import Input from '~/components/Input';
import Button from '~/components/Button';

export const Container = styled.KeyboardAvoidingView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  flex: 1;
`;

export const Header = styled(LinearGradient).attrs({
  colors: ['rgba(255,20,161,1)', '#61045F'],
})``;

export const Subhead = styled.View.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  margin-top: 45px;
  padding: 10px 0;
  background: transparent;
  height: 115px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const Background = styled(LinearGradient).attrs({
  colors: ['#F9F9F9', '#f1f1f1'],
})`
  flex: 1;
`;

export const Panel = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const Avatar = styled.Image`
  width: 80px;
  height: 80px;
  border-radius: 40px;
  border: 3px solid #fff;
`;

export const Card = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 0 50px;
`;

export const Info = styled.View`
  margin-left: 14px;
  display: flex;
`;

export const Label = styled.Text`
  font-size: 10px;
  font-weight: bold;
  line-height: 13px;
  color: #f9f9f9;
`;

export const Description = styled.Text`
  margin-bottom: 4px;
  font-size: 12px;
  line-height: 14px;
  color: #fff;
`;

export const Title = styled.Text`
  margin-bottom: 4px;
  font-size: 16px;
  font-weight: bold;
  line-height: 22px;
  color: #000;
`;

export const List = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
})`
  margin: 5px 0;
  padding: 10px 20px;
`;

export const FormInput = styled(Input)`
  margin-bottom: 2px;
`;

export const SubmitButton = styled(Button)`
  margin-top: 2px;
`;

export const FilterContainer = styled.View`
  margin-top: 50px;
  padding: 8px;
  display: flex;
  align-self: stretch;
`;

export const PickerContainer = styled.View`
  padding: 0 15px;
  background: rgba(0, 0, 0, 0.3);
  border-radius: 4px;
  margin-bottom: 8px;

  flex-direction: row;
  align-items: center;
`;

export const Search = styled.View`
  margin: 0;
  padding: 0 40px;
  height: 54px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background: #61045f;
`;

export const SearchButton = styled.TouchableOpacity`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

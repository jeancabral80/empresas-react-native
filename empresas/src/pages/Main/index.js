import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import LottieView from 'lottie-react-native';

import { ScrollView, Picker } from 'react-native';

import Icon from 'react-native-vector-icons/Feather';
import {
  getAllEnterpriseRequest,
  filterEnterpriseRequest,
} from '~/store/modules/enterprise/actions';

import LoadingList from '~/assets/animations/loading.json';

import {
  Container,
  Header,
  Subhead,
  Avatar,
  Card,
  Title,
  Label,
  Info,
  Description,
  Panel,
  Background,
  List,
  FormInput,
  FilterContainer,
  PickerContainer,
  SubmitButton,
  Search,
  SearchButton,
} from './styles';

import enterpriseTypes from '~/assets/data/enterprise_types.json';

import Enterprise from '~/components/Enterprise';

export default function Main({ navigation }) {
  const dispatch = useDispatch();

  const user = useSelector(state => state.auth);

  const [keyword, setKeyword] = useState('');

  const [filterMode, setFilterMode] = useState(false);

  const [type, setType] = useState({ enterpriseType: 1 });

  const data = useSelector(state => state.enterprise);

  async function toogleFilter() {
    setFilterMode(!filterMode);

    if (filterMode) {
      dispatch(getAllEnterpriseRequest());
    }
  }

  function handleFilter() {
    dispatch(filterEnterpriseRequest(type.enterpriseType, keyword));
  }

  useEffect(() => {
    async function getEnterpriseList() {
      dispatch(getAllEnterpriseRequest());
    }

    getEnterpriseList();
  }, [dispatch, navigation]);

  return (
    <Background>
      <Container>
        <Header>
          {!filterMode && (
            <Subhead>
              <>
                {user && (
                  <Card>
                    <Avatar
                      source={{
                        uri: user.investor.photo
                          ? user.investor.photo
                          : `https://i.pravatar.cc/150?u=${
                              user.investor.email
                            }`,
                      }}
                    />
                    <Info>
                      <Label>Name</Label>
                      <Description>{user.investor.investor_name}</Description>
                      <Label>E-mail</Label>
                      <Description>{user.investor.email}</Description>
                      <Label>Location</Label>
                      <Description>{`${user.investor.city} (${
                        user.investor.country
                      })`}</Description>
                    </Info>
                  </Card>
                )}
              </>
            </Subhead>
          )}

          {filterMode && (
            <FilterContainer>
              <FormInput
                icon="search"
                autocorrect={false}
                autoCapitalize="none"
                placeholder="Enterprise Name"
                value={keyword}
                onChangeText={setKeyword}
              />

              <PickerContainer>
                <Picker
                  selectedValue={type.enterpriseType}
                  style={{
                    width: '100%',
                    color: '#fff',
                    borderRadius: 8,
                  }}
                  onValueChange={itemValue =>
                    setType({ enterpriseType: itemValue })
                  }
                >
                  {enterpriseTypes.enterprise_types.map(eType => {
                    return (
                      <Picker.Item
                        key={eType.id}
                        value={eType.id}
                        label={eType.enterprise_type_name}
                      />
                    );
                  })}
                </Picker>
              </PickerContainer>
              <SubmitButton loading={data.isLoading} onPress={handleFilter}>
                Pesquisar
              </SubmitButton>
            </FilterContainer>
          )}
        </Header>

        <ScrollView>
          {data.isLoading ? (
            <Panel>
              <LottieView
                source={LoadingList}
                style={{ width: 150, height: 84 }}
                autoPlay
                loop
              />
            </Panel>
          ) : (
            <List
              data={Array.from(data.enterprises)}
              keyExtractor={item => String(item.id)}
              renderItem={({ item }) => (
                <Enterprise navigation={navigation} data={item} />
              )}
              ListEmptyComponent={() => (
                <Panel>
                  <Title>No Records</Title>
                </Panel>
              )}
            />
          )}
        </ScrollView>
        <Search>
          <SearchButton onPress={toogleFilter}>
            <Icon name={!filterMode ? 'search' : 'x'} size={20} color="#fff" />
            <Label>{!filterMode ? 'Search' : 'Close'}</Label>
          </SearchButton>
        </Search>
      </Container>
    </Background>
  );
}

Main.navigationOptions = ({ navigation }) => ({
  title: 'ioasys enterprises',
});

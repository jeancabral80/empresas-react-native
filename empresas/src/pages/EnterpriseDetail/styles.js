import { Platform } from 'react-native';
import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';
import { RectButton } from 'react-native-gesture-handler';

export const Container = styled.KeyboardAvoidingView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  flex: 1;
`;

export const Content = styled.ScrollView.attrs({
  contentContainerStyle: { paddingLeft: 10, paddingRight: 20 },
  showsHorizontalScrollIndicator: false,
})`
  padding: 0 10px;
`;

export const Header = styled(LinearGradient).attrs({
  colors: ['rgba(255,20,161,1)', '#61045F'],
})``;

export const Subhead = styled.View.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  margin-top: 45px;
  padding: 10px 0;
  background: transparent;
  height: 115px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const Background = styled(LinearGradient).attrs({
  colors: ['#F9F9F9', '#f1f1f1'],
})`
  flex: 1;
`;

export const Panel = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const Avatar = styled.Image`
  width: 90px;
  height: 90px;
  border-radius: 45px;
`;

export const Card = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 0 20px;
`;

export const Info = styled.View`
  margin-left: 14px;
  display: flex;
`;

export const Label = styled.Text`
  font-size: 9px;
  line-height: 10px;
  color: #f9f9f9;
`;

export const Description = styled.Text`
  margin-bottom: 15px;
  font-size: 16px;
  line-height: 19px;
  color: #61045f;
`;

export const Title = styled.Text`
  margin-bottom: 10px;
  font-size: 24px;
  font-weight: bold;
  color: #fff;
`;

export const List = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
})`
  margin: 5px 0;
  padding: 10px 20px;
`;

export const Photo = styled.Image`
  width: 42px;
  height: 42px;
  border-radius: 21px;
`;

export const Social = styled.View`
  margin: 0;
  padding: 0 40px;
  height: 52px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  background: #61045f;
`;

export const Facts = styled.View`
  margin: 0;
  padding: 0 40px;
  display: flex;
  height: 48px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Fact = styled.View`
  margin: 5px;
  padding: 10px;
  background: transparent;
  flex-grow: 1;
  flex-basis: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

export const ItemName = styled.Text.attrs({
  numberOfLines: 1,
})`
  margin-bottom: 4px;
  font-size: 12px;
  color: #fff;
`;

export const ItemValue = styled.Text`
  margin-bottom: 4px;
  font-weight: bold;
  font-size: 18px;
  color: rgba(255, 20, 161, 1);
`;

export const Heading = styled.Text`
  margin: 10px 0;
  font-size: 22px;
  font-weight: bold;
  color: rgba(255, 20, 161, 1);
`;

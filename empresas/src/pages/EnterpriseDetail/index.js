import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { TouchableOpacity, ActivityIndicator } from 'react-native';

import Icon from 'react-native-vector-icons/Feather';

import {
  Container,
  Title,
  Header,
  Subhead,
  Background,
  Social,
  Panel,
  Facts,
  Fact,
  ItemValue,
  ItemName,
  Content,
  Heading,
  Description,
} from './styles';

import api from '~/services/api';

export default function EnterpriseDetail({ navigation }) {
  const user = useSelector(state => state.auth);

  const [loading, setLoading] = useState(false);

  const [entepriseDetail, setEntepriseDetail] = useState([]);

  useEffect(() => {
    async function getEntepriseList() {
      setLoading(true);

      const config = {
        headers: {
          'access-token': user.accessToken,
          client: user.client,
          uid: user.uid,
        },
      };

      const { id } = navigation.state.params;

      const response = await api.get(`/enterprises/${id}`, config);

      setEntepriseDetail(response.data.enterprise);

      setLoading(false);
    }
    getEntepriseList();
  }, [navigation, user.accessToken, user.client, user.uid]);

  return (
    <Background>
      <Container>
        <Header>
          <Subhead>
            {loading ? (
              <Panel>
                <ActivityIndicator size="small" color="#fff" />
              </Panel>
            ) : (
              <Panel>
                <Title>{entepriseDetail.enterprise_name}</Title>
                <Facts>
                  <Fact>
                    <ItemValue>{entepriseDetail.value}</ItemValue>
                    <ItemName>Value</ItemName>
                  </Fact>
                  <Fact>
                    <ItemValue>{entepriseDetail.shares}</ItemValue>
                    <ItemName>Share</ItemName>
                  </Fact>
                  <Fact>
                    <ItemValue>{entepriseDetail.share_price}</ItemValue>
                    <ItemName>Value Share</ItemName>
                  </Fact>
                </Facts>
              </Panel>
            )}
          </Subhead>
        </Header>
        <Panel>
          {loading ? (
            <ActivityIndicator size="large" color="rgba(255, 20, 161, 1)" />
          ) : (
            <Content>
              <Heading>About Us</Heading>
              <Description>{entepriseDetail.description}</Description>
              <Heading>Bussiness</Heading>
              <Description>
                {navigation.state.params.enterprise_type.enterprise_type_name}
              </Description>
              <Heading>Location</Heading>
              <Description>
                {`${entepriseDetail.city} (${entepriseDetail.country})`}
              </Description>
            </Content>
          )}
        </Panel>
        <Social>
          <TouchableOpacity onPress={() => {}}>
            <Icon name="phone-call" size={20} color="#fff" />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <Icon name="mail" size={20} color="#fff" />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <Icon name="facebook" size={20} color="#fff" />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <Icon name="twitter" size={20} color="#fff" />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <Icon name="linkedin" size={20} color="#fff" />
          </TouchableOpacity>
        </Social>
      </Container>
    </Background>
  );
}

EnterpriseDetail.navigationOptions = ({ navigation }) => ({
  title: `Details`,
});

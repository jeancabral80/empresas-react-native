import React, { useRef, useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import { Image, Dimensions } from 'react-native';
import { signInRequest } from '~/store/modules/auth/actions';

import Background from '~/components/Background';
import Logo from '~/assets/logo.png';

import {
  Container,
  Form,
  FormInput,
  SubmitButton,
  SignLink,
  SignLinkText,
} from './styles';

export default function SignIn({ navigation }) {
  const dispatch = useDispatch();

  const passwordRef = useRef();

  const loading = useSelector(state => state.auth.isLoading);

  const [email, setEmail] = useState('testeapple@ioasys.com.br');
  const [password, setPassword] = useState('12341234');

  function handleSubmit() {
    dispatch(signInRequest(email, password));
  }

  return (
    <Background>
      <Container style={{ flez: 1, padding: 20 }}>
        <Image
          style={{
            alignSelf: 'center',
            width: Dimensions.get('window').height * 0.12 * (1950 / 662),
            height: Dimensions.get('window').height * 0.12,
            marginVertical: Dimensions.get('window').height * 0.01,
          }}
          resizeMode="contain"
          source={Logo}
        />
        <Form>
          <FormInput
            icon="mail"
            keyboard-type="email-address"
            autocorrect={false}
            autoCapitalize="none"
            placeholder="E-mail"
            returnKeyType="next"
            onSubmitEditing={() => passwordRef.current.focus()}
            value={email}
            onChangeText={setEmail}
          />
          <FormInput
            icon="key"
            secureTextEntry
            placeholder="Senha"
            ref={passwordRef}
            returnKeyType="send"
            onSubmitEditing={handleSubmit}
            value={password}
            onChangeText={setPassword}
          />
          <SubmitButton loading={loading} onPress={handleSubmit}>
            Acessar
          </SubmitButton>
          <SignLink onPress={() => {}}>
            <SignLinkText>Criar conta</SignLinkText>
          </SignLink>
        </Form>
      </Container>
    </Background>
  );
}
